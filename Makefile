.PHONY: doc
.DEFAULT_GOAL := help
###########################################################################################################
## SCRIPTS
###########################################################################################################

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
        match = re.match(r'^([,\sa-zA-Z_-]+):.*?## (.*)$$', line)
        if match:
                target, help = match.groups()
                print("%-20s %s" % (target, help))
endef

###########################################################################################################
## VARIABLES
###########################################################################################################

export PRINT_HELP_PYSCRIPT
export PYTHON=python3

###########################################################################################################
## TARGETS
###########################################################################################################

help:
	@$(PYTHON) -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

doc: ## create documentation
	cd doc && $(MAKE) html
	@echo "open 'http://localhost:63342/python_playground/doc/_build/html/index.html' in a web browser"

doc_view: ## host documentation
	cd doc/_build/html && $(PYTHON) -m http.server

clean:  ## delete every artifact
	cd doc && $(MAKE) clean