# Matlab File Access

Access Data from Matlab Files. Deal with Structure-, Table- and maybe Dataset-Format.
Print Code to use in Matlab (just started).

Created for my needs (Load Table: [StackOverflow](https://stackoverflow.com/questions/25853840/load-matlab-tables-in-python-using-scipy-io-loadmat)).
Feedback is welcome!

[TOC]

## Overview

- [demo.py](demo.py): Demonstration of import function.
- [demo_file.mat](demo_file.mat): Example File from Matlab.
- [demo_file_readable.mat](demo_file.mat): Converted File from Matlab.
- [make_mfile_readable.m](make_mfile_readable.m): Matlab function to prepare Matlab File for reading.
- [import_functions.py](import_functions.py): **Functions for the Import.**
  Use `make doc` to create the HTML documentation.
- [print_matlab.py](print_matlab.py): **Functions to write Matlab Code**

## Using

### Import Functions

1. Maybe use **Matlab** to prepare your Matlab File with the `make_mfile_readable` function.
2. Use **Python** and call `print_overview(mdata, recursion=-1)` on a variable `mdata` you have loaded with `scipy.io.loadmat` 
See the [demo.py](demo.py) file.

If everything worked you should see an Overview of you file and can access most of the information via
- `get_variable_names()` / `get_field_names()` to analyze the data
- `load_vector()` to get vector content
- `load_cell_columns()` to get cell content
- `load_table_from_struct()` to get table content

Look into the functions for more explanation.

#### Known Bugs

- Strings are seem to be really problematic.
I didn't found a way to load them.

### Print Functions

- ```
  get_vertical_vector_str([1, 2, 3])
  > '[1; 2; 3]'
  ```

### Todo

- [ ] Recognize strings in top level of File.
- [ ] Improve recursive scanning (e.g. cells)
- [ ] Generalize loading