""" Functions to print commands for Matlab """


def get_vertical_vector_str(vector_list: list) -> str:
    """ Print a list as a vertical matlab vector or array (if list contains list) """

    return '[{}]'.format('; '.join([str(row) for row in vector_list]))
