%% change content of a file to be readable by python
% mfile_filename is string without ".mat"

function make_mfile_readable(mfile_filename)

% load content
variables = load(mfile_filename);
variable_names = fieldnames(variables);

% change content
%   by Brute Force with EVAL (has someone a better idea?)
for ii = 1:length(variable_names)
  
    variables.(variable_names{ii}) = ...
        make_mvar_readable(variables.(variable_names{ii}));
end

% save content
save(mfile_filename + "_readable", '-struct', 'variables')

end


%% change variable to be readable by python
% only relevant for table or dataset

function mvar = make_mvar_readable(mvar)

if class(mvar) == "table"
    mvar = struct('table', struct(mvar), ...
        'columns', {struct(mvar).varDim.labels});
    
elseif class(mvar) == "dataset"
    mvar = struct('table', struct(mvar), ...
        'columns', {get(mvar,'VarNames')});

elseif class(mvar) == "cell" || class(mvar) == "struct"
    
    if length(mvar) > 1
    
        for ii = 1:length(mvar)
            mvar(ii) = make_mvar_readable(mvar(ii));
        end
    
        % everyhing from here on is only one element
        
    elseif class(mvar) == "cell"
        mvar{1} = make_mvar_readable(mvar{1});
            
    elseif class(mvar) == "struct"
        
        fields = fieldnames(mvar);
        for ii = 1:length(fields)
            mvar.(fields{ii}) = make_mvar_readable(mvar.(fields{ii}));
        end
        
    end
elseif any(strcmp(class(mvar), ["double","string"]))
    % everything fine
else
    disp(['Sorry "', class(mvar), '" is unknown => ignored.'])
end

end