""" Functions for handling Matlab Files """

import numpy as np
import pandas as pd


def get_variable_names(mdata) -> list:
    """ Return variable names of a .mat file.

    :param mdata: Loaded .mat file.
    :return: List with variable names.
    """
    names = []
    for name in list(mdata):
        if name[0] != '_':
            names.append(name)
    return sorted(names)


def get_field_names(mdata, variable_name: str) -> list:
    """ Return field names of a structure from a .mat file.

    :param mdata: Loaded .mat file.
    :param variable_name: Name of structure to scan.
    :return: List with fieldnames.
    """
    if mdata[variable_name].dtype.names is None:
        return []
    else:
        return [name for name in mdata[variable_name].dtype.names]


def check4table(mdata) -> bool:
    """ Checks for a prepared structure of table or dataset (see load_table_from_struct).

    :param mdata: Structure in .mat file.
    :return: True if prepared structure is found.
    """
    if len(mdata.shape) == 2 and mdata[0, 0].dtype.names is not None and len(mdata[0, 0].dtype.names) == 2 \
            and "table" in mdata[0, 0].dtype.names and "columns" in mdata[0, 0].dtype.names:
        return True
    else:
        return False


def check4strings(mdata) -> bool:
    if len(mdata.shape) == 1 and isinstance(mdata[0].data, memoryview) and mdata[0].dtype.name == 'void256':
        return True  # probably
    else:
        return False


def get_type(mdata_variable) -> str:
    """ Try to identify the type of a mdata variable """

    if isinstance(mdata_variable, str):
        return "st"
    elif isinstance(mdata_variable, bool):
        return "bool"
    elif isinstance(mdata_variable, int):
        return "int"
    elif isinstance(mdata_variable, float):
        return "float"
    elif check4strings(mdata_variable):
        return 'strings'
    elif check4table(mdata_variable):
        return 'table'
    elif mdata_variable.dtype.names is not None:
        return 'structure'
    else:
        return mdata_variable.dtype.name


def get_content(mdata) -> list:
    """ Scan content of a variable from a .mat file.

    :param mdata: Content of a .mat file.
    :return: List of lists with file types for each entry.
    """
    if len(mdata.shape) > 0 and mdata.shape[0] > 1:
        content = []
        for ii in range(mdata.shape[0]):
            content.append(get_content(mdata[ii]))
        return content
    else:
        return [get_type(mdata)]


def get_content_overview(mdata) -> str:
    """ Scan content of a variable from a .mat file.

    :param mdata: Content of a .mat file.
    :return: String with description of content types.
    """
    if len(mdata.shape) > 0 and mdata.shape[0] > 1:
        content = []
        for ii in range(mdata.shape[0]):
            content.append(get_content_overview(mdata[ii]))
        simple_content = []
        for entry in list(set(content)):
            simple_content.append('{count}x "{entry}"'.format(count=content.count(entry), entry=entry))
        return '[{}]'.format("; ".join(sorted(simple_content)))
    else:
        return get_type(mdata)


def print_variable_overview(mdata, variable_name: str, recursion: int = 0, depth: int = 0,
                            content: bool = False) -> None:
    """ Shows content of a variable (and its content) in Matlab Data Structure.

    :param mdata: (Part of) loaded .mat file.
    :param variable_name: Variable to scan.
    :param recursion: Depth of output (default only top level, negative values for indefinite depth).
    :param depth: Formatting parameter for indent.
    :param content: Try to show content of variables.
    :return: Prints info in console.
    """
    indent = " " * depth
    if depth == 0:
        print('{indent}Info for "{variable_name}" (Fieldname: Shape)'.format(indent=" "*depth,
                                                                             variable_name=variable_name))
    if check4table(mdata[variable_name]):
        print('{indent} This is a table struct with {cols} columns and {rows}? rows.'.format(
            indent=indent, cols=mdata[variable_name][0, 0]['columns'].size,
            rows=mdata[variable_name][0, 0]['table'][0, 0]['data'][0, 0].size))
        if content:
            print("{indent}  Columns: {columns}".format(indent=indent, columns=[
                name[0] for name in mdata[variable_name][0, 0]['columns'][0]]))
            print("{indent}  Content: {content}".format(indent=indent, content=get_content_overview(
                mdata[variable_name][0, 0]['table'][0, 0]['data'][0])))
        return

    field_names = get_field_names(mdata, variable_name)
    for field_name in field_names:
        if len(mdata[variable_name].shape) == 1:
            if content:
                print('{indent} "{name}": [{type}]'.format(indent=" "*depth, name=field_name,
                                                           type=type(mdata[variable_name][field_name][0])))
        else:
            print('{indent} "{name}": {shape}'.format(indent=indent, name=field_name,
                                                      shape=mdata[variable_name][field_name][0, 0].shape))
            if check4strings(mdata[variable_name][0, 0][field_name]):
                print("{indent}  This is probably an array of strings.".format(indent=indent))
            elif recursion != 0:
                if content and mdata[variable_name][0, 0][field_name].dtype.names is None:
                    print(indent, get_content_overview(mdata[variable_name][0, 0][field_name]))
                else:
                    print_variable_overview(mdata[variable_name][0, 0], field_name,
                                            recursion=recursion-1, depth=depth+1, content=content)


def print_overview(mdata, recursion: int = 0, content: bool = False) -> None:
    """ Shows content of Matlab file.

    :param mdata: Loaded .mat file.
    :param recursion: Depth of output (default only top level, negative values for indefinite depth).
    :param content: Try to show content of variables.
    :return: Prints info in console.
    """
    print('Info for matlab Data: (Name: Shape)')
    variable_names = get_variable_names(mdata)
    for name in variable_names:
        print(' "{name}": {shape}'.format(name=name, shape=mdata[name].shape))
        if recursion != 0:
            print_variable_overview(mdata, name, recursion=recursion - 1, depth=1, content=content)


def get_frame_from_data(mdata):  # I probably didn't documented it because it isn't working - who knows
    table_dict = {}
    for name in list(mdata):
        if name[0] != '_':
            if len(mdata[name].shape) > 1:
                if mdata[name].shape[0] == 1:
                    table_dict[name] = mdata[name][0]
                elif mdata[name].shape[1] == 1:
                    table_dict[name] = mdata[name][:, 0]
                else:
                    raise Exception('Data "{name}" is 2D ({x},{y}), this won''t work.'.format(name=name,
                                                                                              x=mdata[name].shape[0],
                                                                                              y=mdata[name].shape[1]))
            else:
                table_dict[name] = mdata[name]

    return pd.DataFrame(table_dict)


def load_vector(vector_variable) -> list:
    """ Load a vector with double or other - trivial, but as example

    :param vector_variable: variable with double data from loaded mfile
    :return: list with content of double variable
    """
    return [val[0] for val in vector_variable]


def load_cell_columns(cell_variable) -> pd.DataFrame:
    """ Load a cell variable with column structure and try to create a dataframe

    This method assumes every column has a consistent data type

    :param cell_variable: somme cell variable from loaded mfile
    :return: DataFrame with content of cell structure
    """

    # Get Data #
    ############

    column_names = []
    column_data = []

    n_columns = cell_variable[0, :].size
    n_rows = cell_variable[:, 0].size

    for i_col in range(n_columns):

        content_type = get_type(cell_variable[:, i_col])

        if content_type == 'strings':
            column_names.append('strings')
            column_data.append([None] * n_rows)
            continue
        elif content_type == 'object':
            element_type = get_type(cell_variable[0, i_col])

            # todo: filter for types
            #   This is just quick and dirty
            column_names.append(element_type)
        else:
            print('Unclear column type: {}! Try anyway.'.format(content_type))

        # scan each entry individually because it could be different

        entry_vec = []
        for entry_field in cell_variable[:, i_col]:

            if entry_field.size == 1:
                entry_vec.append(entry_field[0][0])
            elif isinstance(entry_field, np.ndarray):
                # todo: this is probably bad row and column vectors will be both converted to list.
                #  Does matlab mae a difference?
                if entry_field.shape[0] == 1:
                    entry_vec.append(entry_field[0, :])
                elif entry_field.shape[1] == 1:
                    entry_vec.append(entry_field[:, 0])
                else:
                    entry_vec.append(entry_field)
            else:
                print('Unclear entry type: {}! Add anyway.'.format(type(entry_field)))
                entry_vec.append(entry_field)

        column_data.append(entry_vec)

    # Convert to Frame #
    ####################

    # prepare unique column names
    unique_names = []
    c_names = set(column_names)
    c_counter = {nam: 0 for nam in c_names}
    for c_name in column_names:
        unique_names.append('{nam}_{num}'.format(nam=c_name, num=c_counter[c_name]))
        c_counter[c_name] += 1

    data_fame = pd.DataFrame({cn: cv for cn, cv in zip(unique_names, column_data)})

    return data_fame


def load_table_from_struct(table_structure, drop_short_columns: int = -1) -> pd.DataFrame():
    """ Load a prepared "table" from a matlab file.

    See <https://stackoverflow.com/a/60614560/11514530> for more Information (e.g. load old dataset format).

    Inside Matlab:

        YourVariableName = struct('table', struct(TableYouWantToLoad),
                                  'columns', struct(TableYouWantToLoad).varDim.labels)

        save("YourFileName", "YourVariableName")

    Inside Python:

        import scipy.io as sio
        mdata = sio.loadmat("YourFileName")
        loaded_table = load_table_from_struct(mdata["YourVariableName"])

    Based on the idea of Jochen
    <https://stackoverflow.com/questions/25853840/load-matlab-tables-in-python-using-scipy-io-loadmat>

    :param table_structure: prepared "struct" variable of a .mat file loaded by scipy.io.loadmat

        field "table" with struct(some_table)
        field "columns" with some_table.varDim.labels

    :param drop_short_columns:

        0 will raise error if there are shorter columns
        >0 will drop shorter columns (probably string columns) without error if the number is matching the given number
        -1 will drop short columns and print warning

    :return: Pandas DataFrame with table content

    """

    # get prepared data structure
    data = table_structure[0, 0]['table']['data']
    # get prepared column names
    data_cols = [name[0] for name in table_structure[0, 0]['columns'][0]]

    # create dict out of original table
    table_dict = {}
    for col_id in range(len(data_cols)):
        table_dict[data_cols[col_id]] = [val[0] for val in data[0, 0][0, col_id]]

    # check for shorter columns
    #   string columns have strange format and can't be loaded
    col_lengths, col_counts = np.unique([len(table_dict[col]) for col in table_dict], return_counts=True)
    if len(col_lengths) > 1:
        if drop_short_columns != 0:
            lengths_ranking = [(c_len, c_count) for c_count, c_len in sorted(zip(col_counts, col_lengths),
                                                                             reverse=True)]
            del_columns = []
            for column in table_dict:
                if len(table_dict[column]) < lengths_ranking[0][0]:
                    del_columns.append(column)
            if drop_short_columns < 0:
                print('WARNING: There are {short} columns shorter the than longest with {entries} entries.'.format(
                    short=len(del_columns), entries=lengths_ranking[0][0]),
                    '\n         These columns will be deleted (probably string columns).')
            elif len(del_columns) != drop_short_columns:
                raise Exception('Expected {should} shorter columns but, there were {real}!'.format(
                    should=drop_short_columns, real=len(del_columns)))
            for column in del_columns:
                del table_dict[column]
        else:
            raise Exception('There are {short}  columns shorter than the longest with {} entries.'
                            'These are probably string columns (which can not be handled yet)')

    return pd.DataFrame(table_dict)
