import scipy.io as sio

from import_functions import print_overview, load_table_from_struct

# General

print('Load matlab data.')
mdata = sio.loadmat('demo_file.mat')
mdata_modified = sio.loadmat('demo_file_readable.mat')

print('\nGet an overview of content:')
print_overview(mdata)
print_overview(mdata_modified)

print('\nMore detail are possible:')
print_overview(mdata, recursion=1)

print('\nAnd even More detail are possible:')
print_overview(mdata_modified, recursion=-1, content=True)


# Load table
print('\nLoad a table:')
mtable = load_table_from_struct(mdata_modified['demo_table'])
print(mtable)

# Hint:
print('\nAcess Other Content:')
mvar = mdata_modified['demo_cell'][0, 1]['and'][0, 0]
print(mvar)
# Matlab Variables are always enclosed in a one element List, so you have to do mvar[0] to access the value.

print('\nDone')
